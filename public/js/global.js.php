<script type="text/javascript">
    var apiBaseURL = (route) => {
        let url = '<?php echo Config('app.API_URL'); ?>' + route;
        return url;
    }

    // Datatables Global Function
    var dtFn = {
        getSelectedRowByIdColumn: function(data, id, k) {
            k = (k==undefined)? 'id' : k;
            let arr = {};
            arr[k] = id.toString();
            let returnData = _.find(data, arr);
            if (returnData==undefined) {
                arr[k] = id;
                returnData = _.find(data, arr);
            }
            return returnData;
        }
    }

    var paramsDataTables = {
        buttons: function(cb) {
			return [
				{
					text: 'sssssss',
					action: function ( e, dt, node, config ) {
						cb();
					}
				}
			];
        },
    }
</script>