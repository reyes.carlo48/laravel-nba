<?php

namespace App\Traits;

use __;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

trait ResponseTraits
{
    public function response($arrResponse, $isExit = false)
    {
        $r = response()->json(
            [
                'status_code' => __::get($arrResponse, 'status_code', 400),
                'message' => __::get($arrResponse, 'message', 400),
                'data' => __::get($arrResponse, 'data', []),
            ],
            __::get($arrResponse, 'status_code', 400)
        );
        if ($isExit) {
            response()
                ->json(
                    array_values([
                        'status_code' => __::get(
                            $arrResponse,
                            'status_code',
                            400
                        ),
                        'message' => __::get($arrResponse, 'message', 400),
                        'data' => __::get($arrResponse, 'data', []),
                    ]),
                    $arrResponse['status_code']
                )
                ->send();
            die();
        } else {
            return $r;
        }
    }

    public function getCodeMessage($code)
    {
        $arrCodeMessage = [
            '200' => 'Successfully executed.',
            '200-1' => 'Token generated successfully.',
            '200-2' => 'Data successfully retrieved.',
            '200-3' => 'Token successfully destroyed.',
            '200-4' => 'Token has been successfully refresh.',
            '200-5' => 'Data successfully saved.',
            '200-6' => 'Data successfully deleted.'
        ];

        return __::get($arrCodeMessage, $code, '');
    }
}
