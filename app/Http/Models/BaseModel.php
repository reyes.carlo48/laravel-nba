<?php

namespace App\Http\Models;

use __;
use App\Traits\LoggingTraits;
use App\Traits\ResponseTraits;
use App\Traits\UserLoginTraits;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    /**
     * Return array data based on $key and $value
     *
     * @param string $key Model Column name
     * @param string $value Value to be searched
     *
     * @return array
     */

    public function modelFindBy($key, $value, $visibles=[]): array
    {
        $model = $this->where($key, $value)->get();
        if (count($visibles)) {
            $model->makeVisible($visibles);
        }
        return $model->toArray();
    }

    /*
     * id: model id
     * key: model column name
     * value: value to be updated
     */
    public function modelUpdateBy($id, $key, $value)
    {
        if ($model = $this->find($id)) {
            $model->$key = $value;
            $model->save();
            return $model->toArray();
        }
        return false;
    }

    /**
     * Update table by id thru array value pair data (column => value)
     * Return data array if sucessfully updated
     * Return null array if not successfully updated
     *
     * @param integer $id Model Id
     * @param array $data Key value pair of Column-Value
     *
     * @return array
     */

    public function modelUpdateById($id, $data = []): array
    {
        $results = [];
        try {
            if ($model = $this->find($id)) {
                foreach ($data as $key => $value) {
                    $model->$key = $value;
                }
                $model->save();
                return $model->toArray();
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $msgs = [
                'error' => $e->getMessage(),
                '__LINE__' => __LINE__,
            ];
            $infos = [
                'error_type' => 'MYSQL',
                'is_error' => 1,
                'chain_id' => 0,
                'api_url' => __FILE__,
                'contents' => json_encode($msgs),
                'remarks' => 'Error during updating records.',
            ];
            $this->log($infos);
        }

        return $results;
    }

    public function deleteDataById($id)
    {
        $collection = self::where('id', $id)->get(['id']);
        self::destroy($collection->toArray());
    }

    public function deleteDataBy($key, $value)
    {
        $collection = self::where($key, $value)->get(['id']);
        self::destroy($collection->toArray());
    }

    public function modelFindByAndWheres($fields)
    {
        $counter = 0;
        foreach ($fields as $field => $value) {
            if ($counter == 0) {
                $Model = self::where($field, '=', $value);
            } else {
                $Model->where($field, '=', $value);
            }
            $counter = 1;
        }
        return $Model->get()->toArray();
    }

    /**
     * Get parent_id of translation.
     *
     * @param int $id 
     *
     * @return integer
     */
    public function getEnglishId($id) {
        $data = self::find($id);  
        $id = __::get($data, 'parent_id', 0) == 0 ? $id : __::get($data, 'parent_id', null);
        return $id;
    }  
}
