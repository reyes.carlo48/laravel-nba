<?php

namespace App\Http\Repositories\Roster;

use __;
use DB;
use DataTables;
use App\Http\Models\Roster;
use App\Http\Repositories\BaseRepository;

class SearchRepository extends BaseRepository
{
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Roster;
    }

    public function _execute($request)
    {
        $Model = $this->model->select('*');

        return DataTables::of($Model)->make();
    }
}
