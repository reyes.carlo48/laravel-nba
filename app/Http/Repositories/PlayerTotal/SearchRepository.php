<?php

namespace App\Http\Repositories\PlayerTotal;

use __;
use DB;
use DataTables;
use App\Http\Models\PlayerTotal;
use App\Http\Repositories\BaseRepository;

class SearchRepository extends BaseRepository
{
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new PlayerTotal;
    }

    public function _execute($request)
    {
        $Model = $this->model->select('*');
        
        $Model->where('player_id', $request->input('player_id'));
        return DataTables::of($Model)->make();
    }
}
