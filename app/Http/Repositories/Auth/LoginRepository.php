<?php

namespace App\Http\Repositories\Auth;

use __;
use JWTAuth;
use JWTFactory;
use Illuminate\Support\Facades\Auth;
use App\Http\Repositories\BaseRepository;

class LoginRepository extends BaseRepository
{
    protected $model;

    public function __construct()
    {
        parent::__construct();

        Auth::guard()
            ->factory()
            ->setTTL(config('jwt.ttl'));
    }

    public function _execute($request)
    {
        $credentials = request(['username', 'password']);
 
        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    
        return $this->respondWithToken($token);
    }

    /**
        * Get the token array structure.
        *
        * @param  string $token
        *
        * @return \Illuminate\Http\JsonResponse
        */
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }
}
