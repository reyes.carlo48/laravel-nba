<?php
/*
\App\Http\Requests\User\SearchRequest $request
*/

namespace App\Http\Requests\Roster;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize(): bool
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(Request $request): array
    {
        return [
          
        ];
    }

    /**
    * Get the validation messages
    *
    * @return array
    */
    public function messages(): array
    {
        return [
        ];
    }
}