<?php

namespace App\Http\Requests\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'username' => [
                'required',
                (new \App\Http\Rules\Auth\Login\UsernameRule($request))
            ],
            'password' => ['required']
        ];
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }

}
