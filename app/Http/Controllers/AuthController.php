<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Models\User;

class AuthController extends Controller
{
    /**
      * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(\App\Http\Requests\Auth\LoginRequest $request)
    {
         return $this->response([
              'status_code' => 200,
              'data' => (new \App\Http\Repositories\Auth\LoginRepository())->_execute($request),
         ]);
    }
}
