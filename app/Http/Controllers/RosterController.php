<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RosterController extends Controller
{

    /**
     * Show lists of rosters.
     *
     * @return array
     */
    public function search(\App\Http\Requests\Roster\SearchRequest $request)
    {
        return (new \App\Http\Repositories\Roster\SearchRepository())->_execute($request);
    }
}
