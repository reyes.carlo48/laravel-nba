<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlayerTotalController extends Controller
{

    /**
     * Show lists of rosters.
     *
     * @return array
     */
    public function search(Request $request)
    {
        return (new \App\Http\Repositories\PlayerTotal\SearchRepository())->_execute($request);
    }
}
