<?php

namespace App\Http\Rules\Auth\Login;


use __;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\ImplicitRule;


class UsernameRule implements ImplicitRule
{
    private $request;
    private $error;

    public function __construct($request)
    {
        $this->request = $request;
        $this->error = "The :attribute field is invalid.";
    }

    /**
    * return false if there is an error
    * return true if NO error
    *
    * @return boolean
    */
    public function passes($attribute, $value)
    {
        if($this->isValidUsernamePassword()){
            return true;
        }

        return false;
    }

    /**
    * Get the validation messages
    *
    * @return string
    */
    public function message()
    {
        return $this->error;
    }

    /**
     * Check if username and password are valid.
     * 
     * @return bool
     */
    private function isValidUsernamePassword()
    {
        $credentials = $this->request->only('username', 'password');

        if (Auth::guard()->attempt($credentials)) {
            return true;
        }
        else {
            $this->error = "Invalid credentials";
            return false;
        }
    }
}