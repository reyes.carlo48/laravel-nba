<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Roster [ROUTES]
Route::group([
    'prefix'=>'roster',
], function(){
    Route::post('search','RosterController@search');
});

// User [ROUTES]
Route::group([
    'prefix'=>'player_total',
], function(){
    Route::post('search','PlayerTotalController@search');
});

